<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Province extends Model
{
    // Membuat mengarah pada table provinces
    protected $table = 'provinces';
}
