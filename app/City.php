<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    // Membuat mengarah pada table cities
    protected $table = 'cities';

}
